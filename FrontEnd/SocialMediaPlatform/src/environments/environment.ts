// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  // For Firebase JS SDK v7.20.0 and later, measurementId is optional
  firebaseConfig: {
    apiKey: "AIzaSyDQoG2VaifDABWiZn1LEI1HjcbISfTzrdQ",
    authDomain: "socialmediaplatform-cf33a.firebaseapp.com",
    projectId: "socialmediaplatform-cf33a",
    storageBucket: "socialmediaplatform-cf33a.appspot.com",
    messagingSenderId: "845818638430",
    appId: "1:845818638430:web:4ee532084c20a24ee5c4cf",
    measurementId: "G-NT5DY65BHG"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
