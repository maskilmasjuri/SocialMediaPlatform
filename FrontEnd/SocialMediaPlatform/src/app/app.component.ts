import { Component } from '@angular/core';

import { MatBottomSheet } from '@angular/material/bottom-sheet';

import { AuthenticatorComponent, AuthenticatorComponentState } from './authenticator/authenticator.component';

import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'SocialMediaPlatform';
  auth = new FirebaseTSAuth()

  constructor(private loginsheet: MatBottomSheet) {
    this.auth.listenToSignInStateChanges(
      user=>{
        this.auth.checkSignInState({
          whenSignedIn:user=>{
            alert("You are logged in!")
          },
          whenSignedOut:user=>{
            alert("You have logged out")
          },
          whenSignedInAndEmailNotVerified:user=>{

          },
          whenSignedInAndEmailVerified:user=>{

          },
          whenChanged:user=>{

          }
        })
      }
    )
  }

  loggedIn(){
    return this.auth.isSignedIn()
  }

  loginClick() {
    this.loginsheet.open(AuthenticatorComponent)
    let state = AuthenticatorComponentState.LOGIN
  }

  onLogoutClick(){
    this.auth.signOut()
  }

}
