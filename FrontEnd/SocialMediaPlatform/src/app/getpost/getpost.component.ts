import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { PostserviceService } from '../postservice.service';

@Component({
  selector: 'app-getpost',
  templateUrl: './getpost.component.html',
  styleUrls: ['./getpost.component.css']
})
export class GetpostComponent implements OnInit {
  posts:any;
  
  constructor(private postService:PostserviceService) { }

  ngOnInit(): void {
    this.postService.getPost().subscribe(data=>{
      console.log(data)
      this.posts=data.valueOf()
    })
  }

}
