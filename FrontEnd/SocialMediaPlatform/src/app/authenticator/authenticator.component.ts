import { Component, OnInit } from '@angular/core';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth'
import { MatBottomSheetRef } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-authenticator',
  templateUrl: './authenticator.component.html',
  styleUrls: ['./authenticator.component.css']
})
export class AuthenticatorComponent implements OnInit {

  state = AuthenticatorComponentState.REGISTER
  firebasetsAuth: FirebaseTSAuth

  constructor(private bottomSheetRef:MatBottomSheetRef) {
    this.firebasetsAuth = new FirebaseTSAuth()
  }

  ngOnInit(): void {
  }

  isLoginState() {
    return this.state == AuthenticatorComponentState.LOGIN
  }
  isRegisterState() {
    return this.state == AuthenticatorComponentState.REGISTER
  }

  isForgotState() {
    return this.state == AuthenticatorComponentState.FORGOT
  }

  loginPage() {
    this.state = AuthenticatorComponentState.LOGIN
  }
  registerPage() {
    this.state = AuthenticatorComponentState.REGISTER
  }
  forgotPage() {
    this.state = AuthenticatorComponentState.FORGOT
  }

  getStateTextHeading() {
    switch (this.state) {
      case AuthenticatorComponentState.LOGIN:
        return "Login"
      case AuthenticatorComponentState.REGISTER:
        return "Create a New Account"
      case AuthenticatorComponentState.FORGOT:
        return "Reset Password"
      default:
        return "Hello!"
    }
  }

  onRegisterClick(registerEmail: HTMLInputElement,
    registerPassword: HTMLInputElement,
    registerConfirmPassword: HTMLInputElement
  ) {
    let email = registerEmail.value
    let password = registerPassword.value
    let confirmPassword = registerConfirmPassword.value
    if (this.isNotEmpty(email) &&
      this.isNotEmpty(password) &&
      this.isMatching(password, confirmPassword)) {
      this.firebasetsAuth.createAccountWith({
        email: email,
        password: password,
        onComplete: (uc) => {
          // alert("Account has been created!")
          // registerEmail.value = ""
          // registerPassword.value = ""
          // registerConfirmPassword.value = ""
          this.bottomSheetRef.dismiss()
        },
        onFail: (err) => {
          alert("Failed to register :(")
        }
      })
    }
    else if (!this.isMatching(password, confirmPassword)) {
      alert("Passwords don't match!")
    }


  }

  onLoginClick(
    loginEmail: HTMLInputElement,
    loginPassword: HTMLInputElement
  ) {
    let email=loginEmail.value
    let password=loginPassword.value
    if(this.isNotEmpty(email) && this.isNotEmpty(password)){
      this.firebasetsAuth.signInWith({
        email:email,
        password:password,
        onComplete: (uc)=>{
          // alert("Logged in successfully")
          // loginEmail.value=""
          // loginPassword.value=""
          this.bottomSheetRef.dismiss()
        },
        onFail: (err)=>{
        alert(err)
        }
      })
    }
  }

  onForgotClick(resetEmail:HTMLInputElement){
    let email=resetEmail.value
    if(this.isNotEmpty(email)){
      this.firebasetsAuth.sendPasswordResetEmail({
        email:email,
        onComplete: (err)=>{
          // alert("Reset password email sent to " + email)
          this.bottomSheetRef.dismiss()
        }
      })
    }
  }

  isNotEmpty(text: string) {
    return text != null && text.length > 0
  }

  isMatching(text: string, compareWith: string) {
    return text == compareWith
  }
}

export enum AuthenticatorComponentState {
  LOGIN,
  REGISTER,
  FORGOT
}
