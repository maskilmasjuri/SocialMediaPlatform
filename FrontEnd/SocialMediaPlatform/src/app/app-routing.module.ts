import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GetpostComponent } from './getpost/getpost.component';
import { HomeComponent } from './home/home.component';
import { PostfeedComponent } from './postfeed/postfeed.component';

const routes: Routes = [
  {
    path: 'posts',
    component: GetpostComponent
  },
  {
    path: 'home',
    component: HomeComponent
  },
  {
    path:'post',
    component:PostfeedComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
