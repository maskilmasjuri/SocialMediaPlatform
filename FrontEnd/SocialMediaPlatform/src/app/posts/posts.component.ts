import { Component, OnInit } from '@angular/core';
import { FirebaseTSAuth } from 'firebasets/firebasetsAuth/firebaseTSAuth';
import { FirebaseTSFirestore } from 'firebasets/firebasetsFirestore/firebaseTSFirestore';
import { FirebaseTSStorage } from 'firebasets/firebasetsStorage/firebaseTSStorage';
import { FirebaseTSApp } from 'firebasets/firebasetsApp/firebaseTSApp';
import { MatDialogRef } from '@angular/material/dialog';


@Component({
  selector: 'app-posts',
  templateUrl: './posts.component.html',
  styleUrls: ['./posts.component.css']
})
export class PostsComponent implements OnInit {

  selectImageFile!: File;
  auth = new FirebaseTSAuth()
  firestore = new FirebaseTSFirestore()
  storage = new FirebaseTSStorage()

  constructor(private dialog: MatDialogRef<PostsComponent>) {

  }

  ngOnInit(): void {
  }

  onPostClick(commentInput: HTMLTextAreaElement) {
    let comment = commentInput.value;
    if(comment.length<=0) return;
    if(this.selectImageFile){
      this.uploadImagePost(comment)
    }
    else {
      this.uploadPost(comment)
    }
  }

  uploadImagePost(comment: string) {
    let postId = this.firestore.genDocId();
    this.storage.upload(
      {
        uploadName: "Upload Image Post",
        path: ["post", postId, "image"],
        data: {
          data: this.selectImageFile
        },
        onComplete: (downloadUrl) => {
          // alert(downloadUrl)
          this.firestore.create({
            path: ["post", postId],
            data: {
              comment: comment,
              createId: this.auth.getAuth().currentUser!.uid,
              imageUrl: downloadUrl,
              timestamp: FirebaseTSApp.getFirestoreTimestamp()
            },
            onComplete: (docId) => {
              this.dialog.close()
            }
          })
        }
      }
    )
  }

  uploadPost(comment: string) {
    this.firestore.create({
      path: ["post"],
      data: {
        comment: comment,
        createId: this.auth.getAuth().currentUser!.uid,
        timestamp: FirebaseTSApp.getFirestoreTimestamp()
      },
      onComplete: (docId) => {
        this.dialog.close()
      }
    })
  }

  onPhotoSelected(photoSelector: HTMLInputElement) {
    this.selectImageFile = photoSelector.files[0]
    let fileReader = new FileReader()
    fileReader.readAsDataURL(this.selectImageFile)
    fileReader.addEventListener(
      "loadend",
      ev => {
        let readableString = fileReader.result!.toString()
        let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image")
        postPreviewImage.src = readableString
      }
    )
  }



}
