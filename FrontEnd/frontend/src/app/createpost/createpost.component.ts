import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';
import { PostService } from '../_services/post.service';
import { MatDialogRef } from '@angular/material/dialog';


import { HttpEventType, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { FileUploadService } from '../_services/file-upload.service';

@Component({
  selector: 'app-createpost',
  templateUrl: './createpost.component.html',
  styleUrls: ['./createpost.component.css']
})
export class CreatepostComponent implements OnInit {
  userid: any;
  isLoggedIn = false;

  selectedImageFile?: File;

  postid: any;

  // File Upload stuff
  message = '';
  fileName = 'Select File';
  fileInfos?: Observable<any>;
  progress = 0;

  
  constructor(private dialog: MatDialogRef<CreatepostComponent>, private postService: PostService, private sessionStorageService: SessionStorageService, private authService:AuthService,private uploadService: FileUploadService) { }

  ngOnInit(): void {
    this.isLoggedIn = this.sessionStorageService.isLoggedIn();
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.userid= user.id;
    }

    this.fileInfos = this.uploadService.getFiles();
  }

  onSubmitPostClick(titleInput: HTMLInputElement, captionInput: HTMLTextAreaElement, hyperlinkInput:HTMLInputElement) {
    let title = titleInput.value;
    let caption = captionInput.value;
    let hyperlink = hyperlinkInput.value;

    if (title.length <= 0) return;
    if (caption.length <= 0) return;

    this.uploadPost(this.userid, title, caption, hyperlink)
  }

  uploadPost(userid: number, title: string, caption: string, hyperlink:string) {
    this.postService.createPost<CreatePostResponse>(userid, title, caption, hyperlink).subscribe({
      next: data=> {

        this.postid = data.id;
        this.dialog.close();
      
        this.upload();
        this.reloadPage();
      }
    })   
  }

  reloadPage(): void {
    window.location.reload();
  }

  onPhotoSelected(photoSelector: HTMLInputElement) {
    this.selectedImageFile = photoSelector.files[0]
    this.fileName = this.selectedImageFile.name;
    let fileReader = new FileReader()
    fileReader.readAsDataURL(this.selectedImageFile)
    fileReader.addEventListener(
      "loadend",
      ev => {
        let readableString = fileReader.result!.toString()
        let postPreviewImage = <HTMLImageElement>document.getElementById("post-preview-image")
        postPreviewImage.src = readableString
      }
    )
  }

  // File Upload stuff
  selectFile(event: any): void {
    if (event.target.files && event.target.files[0]) {
      const file: File = event.target.files[0];
      this.selectedImageFile = file;
      this.fileName = this.selectedImageFile.name;
    } else {
      this.fileName = 'Select File';
    }
  }

  upload(): void {
    this.message = "";

    if (this.selectedImageFile) {
      this.uploadService.upload(this.selectedImageFile,this.postid).subscribe(
        (event: any) => {
          if (event.type === HttpEventType.UploadProgress) {
            this.progress = Math.round(100 * event.loaded / event.total);
          } else if (event instanceof HttpResponse) {
            this.message = event.body.message;
            this.fileInfos = this.uploadService.getFiles();
          }
        },
        (err: any) => {
          console.log(err);

          if (err.error && err.error.message) {
            this.message = err.error.message;
          } else {
            this.message = 'Could not upload the file!';
          }

          this.selectedImageFile = undefined;
        });
    }

  }
}

export interface CreatePostResponse {
  caption: string,
  id: number,
  title: string,
  hyperlink: string
}