import { Component, OnInit } from '@angular/core';
import { CreatepostComponent } from '../createpost/createpost.component';

import { MatDialog } from '@angular/material/dialog';
import { PostService } from '../_services/post.service';

import { Post } from '../_models/post.model';
import { PageEvent } from '@angular/material/paginator';

import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';

@Component({
  selector: 'app-postfeed',
  templateUrl: './postfeed.component.html',
  styleUrls: ['./postfeed.component.css']
})
export class PostfeedComponent implements OnInit {
  posts: Post[] = [];
  
  totalElements: number = 0;

  form: any = {
    title: null,
    caption: null
  };

  isLoggedIn = false;
  errorMessage = '';

  isAdmin = false;
  private roles: string[] = [];

  constructor(private dialog: MatDialog, private postService: PostService, private sessionStorageService: SessionStorageService) {

  }

  ngOnInit(): void {
    this.getPosts({ page: "0" })

    if (this.sessionStorageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.roles = user.roles;

      this.isAdmin = this.roles.includes('ROLE_ADMIN');
    }
  }

  getPosts(request: any) {
    this.postService.getPost(request).subscribe(data => {
      console.log(data);
      this.posts = data['content'];
      this.totalElements = data['totalElements'];
    }, error => {
      console.log(error.error.message);
    });
  }

  nextPage(event: PageEvent) {
    const request:any = {};
    request['pageNo'] = event.pageIndex.toString();
    request['']
    this.getPosts(request);
  }

  onCreatePostClick() {
    this.dialog.open(CreatepostComponent)
  }

}

export interface PostData {
  id: number;
  title: string;
  caption: string;
  hyperlink: string;
  views: number;
  imageUrl?: string;
}
