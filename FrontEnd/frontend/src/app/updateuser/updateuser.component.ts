import { Component,Inject, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';
import { UserService } from '../_services/user.service';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-updateuser',
  template:'passed in {{data.uid}}',
  templateUrl: './updateuser.component.html',
  styleUrls: ['./updateuser.component.css']
})
export class UpdateuserComponent implements OnInit {

  isLoggedIn = false;
  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data:{uid:number},private userService: UserService, private sessionStorageService: SessionStorageService, private authService: AuthService) { }
  userid = this.data.uid
  ngOnInit(): void {
    this.isLoggedIn = this.sessionStorageService.isLoggedIn();
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
    }
  }

  onUpdateClick(usernameInput: HTMLInputElement, emailInput: HTMLInputElement) {
    let username = usernameInput.value;
    let email = emailInput.value;

    if (username.length <= 0) return;
    if (email.length <= 0) return;

    this.updateUser(this.userid, username, email)
  }

  updateUser(userid: number, username: string, email: string) {
    this.userService.updateUser(userid,username,email).subscribe({
      next: data=> {
        this.reloadPage();
      }
    })
  }

  reloadPage(): void {
    window.location.reload();
  }

}
