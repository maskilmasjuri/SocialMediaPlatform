import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UserdisplaypostComponent } from './userdisplaypost.component';

describe('UserdisplaypostComponent', () => {
  let component: UserdisplaypostComponent;
  let fixture: ComponentFixture<UserdisplaypostComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UserdisplaypostComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UserdisplaypostComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
