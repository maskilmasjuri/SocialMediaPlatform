import { Component, Input, OnInit } from '@angular/core';
import { PostData } from '../postfeed/postfeed.component';
import { Post } from '../_models/post.model';

import { SessionStorageService } from '../_services/session-storage.service';
import { PostService } from '../_services/post.service';

import { UpdatepostComponent } from '../updatepost/updatepost.component';
import { MatBottomSheet } from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-userdisplaypost',
  templateUrl: './userdisplaypost.component.html',
  styleUrls: ['./userdisplaypost.component.css']
})
export class UserdisplaypostComponent implements OnInit {
  @Input() postData!: PostData;
  @Input() post!: Post;
  
  isLoggedIn = false;
  isAdmin = false;
  isUser = false;
  private roles: string[] = [];

  constructor(private updatepostSheet: MatBottomSheet, private sessionStorageService: SessionStorageService, private postService: PostService) { }

  ngOnInit(): void {
    if (this.sessionStorageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.roles = user.roles;

      this.isAdmin = this.roles.includes('ROLE_ADMIN');
    }
  }

  onUpdateClick(postid: number) {
    this.updatepostSheet.open(UpdatepostComponent, {data: {pid:postid}})
  }

  onDeleteClick(postid: number) {
    this.postService.deletePost(postid).subscribe(data => {
      console.log(data)

    })
    window.location.reload();
  }

  reloadPage(): void {
    window.location.reload();
  }

}
