import { Component, OnInit } from '@angular/core';
import { CreatepostComponent } from '../createpost/createpost.component';

import { MatDialog } from '@angular/material/dialog';
import { PostService } from '../_services/post.service';

import { Post } from '../_models/post.model';
import { PageEvent } from '@angular/material/paginator';

import { SessionStorageService } from '../_services/session-storage.service';

@Component({
  selector: 'app-userfeed',
  templateUrl: './userfeed.component.html',
  styleUrls: ['./userfeed.component.css']
})
export class UserfeedComponent implements OnInit {
  posts: Post[] = [];
  
  totalElements: number = 0;
  form: any = {
    title: null,
    caption: null
  };

  isLoggedIn = false;
  errorMessage = '';

  isAdmin = false;
  private roles: string[] = [];
  userid: any;

  constructor(private dialog: MatDialog, private postService: PostService, private sessionStorageService: SessionStorageService) { }

  ngOnInit(): void {
   
    if (this.sessionStorageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.roles = user.roles;
      this.userid = user.id;

      this.isAdmin = this.roles.includes('ROLE_ADMIN');
    }
    
    this.getPostByUserId(this.userid, { page: "0" })
  }

  getPostByUserId(userid:number, request: any) {
    this.postService.getPostByUserId(userid,request).subscribe(data => {
      console.log(data);
      this.posts = (data as any)['content'];
      this.totalElements = (data as any)['totalElements'];
    }, error => {
      console.log(error.error.message);
    });
  }

  nextPage(event: PageEvent) {
    const request:any = {};
    request['pageNo'] = event.pageIndex.toString();
    request['']
    this.getPostByUserId(this.userid,request);
  }

  onCreatePostClick() {
    this.dialog.open(CreatepostComponent)
  }

}

export interface PostData {
  id: number;
  title: string;
  caption: string;
  hyperlink: string;
  imageUrl?: string;
}