import { Component, Inject, OnInit } from '@angular/core';
import { PostData } from '../postfeed/postfeed.component';

import { SessionStorageService } from '../_services/session-storage.service';
import { PostService } from '../_services/post.service';

import { MatDialogRef } from '@angular/material/dialog';
import {MAT_BOTTOM_SHEET_DATA} from '@angular/material/bottom-sheet';

@Component({
  selector: 'app-updatepost',
  templateUrl: './updatepost.component.html',
  styleUrls: ['./updatepost.component.css']
})
export class UpdatepostComponent implements OnInit {
  isLoggedIn = false;
  isAdmin = false;
  private roles: string[] = [];

  constructor(@Inject(MAT_BOTTOM_SHEET_DATA) public data:{pid:number}, private sessionStorageService: SessionStorageService, private postService: PostService) { }
  postid = this.data.pid;
  
  ngOnInit(): void {
    if (this.sessionStorageService.isLoggedIn()) {
      this.isLoggedIn = true;
    }
    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.roles = user.roles;

      this.isAdmin = this.roles.includes('ROLE_ADMIN');
    }
  }

  onUpdateClick(titleInput: HTMLInputElement, hyperlinkInput: HTMLInputElement, captionInput: HTMLTextAreaElement) {
    let title = titleInput.value;
    let caption = captionInput.value;
    let hyperlink = hyperlinkInput.value;

    if (title.length <= 0) return;
    if (caption.length <= 0) return;

    this.updatePost(this.postid, title, caption, hyperlink)
  }

  updatePost(postid: number, title: string, caption: string, hyperlink: string) {
    this.postService.updatePost(postid, title, caption, hyperlink).subscribe({
      next: data => {
        this.reloadPage();
      }
    })
  }

  
  reloadPage(): void {
    window.location.reload();
  }

}
