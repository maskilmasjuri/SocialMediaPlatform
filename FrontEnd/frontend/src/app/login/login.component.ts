import { Component, OnInit } from '@angular/core';
import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';

import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { ForgotpasswordComponent } from '../forgotpassword/forgotpassword.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  form: any = {
    username: null,
    password: null
  };
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];

  constructor(private authService: AuthService, private sessionStorageService: SessionStorageService,private forgotsheet: MatBottomSheet) { }

  ngOnInit(): void {
    if (this.sessionStorageService.isLoggedIn()) {
      this.isLoggedIn = true;
      this.roles = this.sessionStorageService.getUser().roles;
    }
  }

  onSubmit(): void {
    const { username, password } = this.form;

    this.authService.login(username, password).subscribe({
      next: data => {
        this.sessionStorageService.saveUser(data);

        this.isLoginFailed = false;
        this.isLoggedIn = true;
        this.roles = this.sessionStorageService.getUser().roles;
        this.reloadPage();
      },
      error: err => {
        this.errorMessage = err.error.message;
        this.isLoginFailed = true;
      }
    });
  }

  reloadPage(): void {
    window.location.reload();
  }

  forgotClick(){   
    this.forgotsheet.open(ForgotpasswordComponent)
  }
}
