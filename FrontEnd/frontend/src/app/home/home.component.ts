import { Component, OnInit } from '@angular/core';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { RegisterComponent } from '../register/register.component';
import { UserService } from '../_services/user.service';

import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
 
  content?: string

  isLoggedIn = false;
  username?: string;

  constructor(private registersheet: MatBottomSheet, private userService:UserService,private sessionStorageService: SessionStorageService, private authService: AuthService) { }

  ngOnInit(): void {

    this.isLoggedIn = this.sessionStorageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.username = user.username;
    }
    
    this.userService.getPublicContent().subscribe({
      next: data => {
        this.content = data;
      },
      error: err => {console.log(err)
        if (err.error) {
          this.content = JSON.parse(err.error).message;
        } else {
          this.content = "Error with status: " + err.status;
        }
      }
    });
  }
  registerClick() {
    this.registersheet.open(RegisterComponent)
  }

}
