import { Component } from '@angular/core';
import { AuthService } from './_services/auth.service';
import { SessionStorageService } from './_services/session-storage.service';

import { MatBottomSheet } from '@angular/material/bottom-sheet';

import { LoginComponent } from './login/login.component';


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'frontend';

  private roles: string[] = [];
  isLoggedIn = false;
  showAdminBoard = false;
  username?: string;

  constructor(private loginsheet: MatBottomSheet,private sessionStorageService: SessionStorageService, private authService: AuthService) {}
  
  ngOnInit(): void {
    this.isLoggedIn = this.sessionStorageService.isLoggedIn();

    if (this.isLoggedIn) {
      const user = this.sessionStorageService.getUser();
      this.roles = user.roles;

      this.showAdminBoard = this.roles.includes('ROLE_ADMIN');

      this.username = user.username;
    }
  }
  logoutClick(): void {
    this.authService.logout().subscribe({
      next: res => {
        console.log(res);
        this.sessionStorageService.clean();

        window.location.reload();
      },
      error: err => {
        console.log(err);
      }
    })
  }

  loginClick() {
    this.loginsheet.open(LoginComponent)
  }
}
