import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { RegisterComponent } from './register/register.component';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { ProfileComponent } from './profile/profile.component';
import { BoardUserComponent } from './board-user/board-user.component';
import { BoardAdminComponent } from './board-admin/board-admin.component';
import { PostfeedComponent } from './postfeed/postfeed.component';
import { UserfeedComponent } from './userfeed/userfeed.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';

const routes: Routes = [{
  path: 'home',
  component: HomeComponent
},
{
  path: 'register',
  component: RegisterComponent
},
{
  path: 'login',
  component: LoginComponent
},
{
  path: 'profile',
  component: ProfileComponent
},
{
  path: 'user',
  component: BoardUserComponent
},
{
  path: 'admin',
  component: BoardAdminComponent
},
{
  path: 'postfeed',
  component: PostfeedComponent
},
{
  path: 'userfeed',
  component: UserfeedComponent
},
{
  path: 'forgotpassword',
  component: ForgotpasswordComponent
},
{
  path: '', redirectTo: 'home',
  pathMatch: 'full'
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
