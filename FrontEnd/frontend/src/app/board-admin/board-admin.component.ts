import { Component, OnInit } from '@angular/core';
import { UserService } from '../_services/user.service';

import { MatBottomSheet } from '@angular/material/bottom-sheet';

import { AuthService } from '../_services/auth.service';
import { SessionStorageService } from '../_services/session-storage.service';
import { PostService } from '../_services/post.service';
import { UpdateuserComponent } from '../updateuser/updateuser.component';

@Component({
  selector: 'app-board-admin',
  templateUrl: './board-admin.component.html',
  styleUrls: ['./board-admin.component.css']
})
export class BoardAdminComponent implements OnInit {
  users: any;
  isAdmin:boolean= false;
  sessionid:any;

  constructor(private updateuserSheet: MatBottomSheet, private userService: UserService, private sessionStorageService:SessionStorageService) { }

  ngOnInit(): void {
    this.userService.getAllUsers().subscribe(data => {
      console.log(data)
      this.users = data.valueOf()
    })
    this.sessionid = this.sessionStorageService.getUser().id
    if(this.sessionid == 1){
      this.isAdmin = true;
    }
  }

  onDeleteClick(userid: number) {
    this.userService.deleteUser(userid).subscribe(data => {
      console.log(data)
      
    })
    window.location.reload();
  }

  openUpdateClick(userid: number){
  
    this.updateuserSheet.open(UpdateuserComponent, {data: {uid:userid}})
  }
 

}
