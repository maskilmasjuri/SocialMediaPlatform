import { Component, OnInit } from '@angular/core';
import { SessionStorageService } from '../_services/session-storage.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
  currentUser: any;

  constructor(private sessionStorageService: SessionStorageService) { }

  ngOnInit(): void {
    this.currentUser = this.sessionStorageService.getUser();
  }

}
