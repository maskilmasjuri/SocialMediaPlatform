import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

import { Post } from '../_models/post.model';

import { CreatePostResponse } from '../createpost/createpost.component';


const API_URL = 'http://localhost:8080/api/';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http: HttpClient) { }

  getPost(request:any): Observable<any> {
    const params = request;
    return this.http.get(API_URL + 'posts', {params})
  }
  // getPostPage(pageNo:number) {
  //   return this.http.get(API_URL + 'posts?pageNo=' + pageNo)
  // }

  // getPost(params: any): Observable<any>  {
  //   return this.http.get(API_URL + 'posts' + { params })
  // }

  getPostByUserId(userid: number, request:any) {
    const params = request;
    return this.http.get(API_URL + 'users/' + userid + '/posts', {params})
  }

  createPost<CreatePostResponse>(userid: number, title: string, caption: string, hyperlink: string): Observable<any> {
    return this.http.post(
      API_URL + 'users/' + userid + '/posts',
      {
        title,
        caption,
        hyperlink
      },
      httpOptions
    )
  }

  updatePost(postid: number, title: string, caption: string, hyperlink: string) {
    return this.http.put(
      API_URL + 'posts/' + postid,
      {
        title,
        caption,
        hyperlink
      },
      httpOptions
    )
  }

  updatePostViews(postid: number) {
    return this.http.put(API_URL + 'posts/views/' + postid, {}, httpOptions)
  }

  deletePost(postid: number) {
    return this.http.delete(API_URL + 'posts/' + postid)
  }
}
