package com.dxc.payload;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostResponse {
	private List<PostDTO> content;
	private int pageNo;
	private int pageSize;
	private long totalElements;
	private long totalPages;
	private boolean last;
	public List<PostDTO> getContent() {
		return content;
	}
	public int getPageNo() {
		return pageNo;
	}
	public int getPageSize() {
		return pageSize;
	}
	public long getTotalElements() {
		return totalElements;
	}
	public long getTotalPages() {
		return totalPages;
	}
	public boolean isLast() {
		return last;
	}
	public void setContent(List<PostDTO> content) {
		this.content = content;
	}
	public void setPageNo(int pageNo) {
		this.pageNo = pageNo;
	}
	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}
	public void setTotalElements(long totalElements) {
		this.totalElements = totalElements;
	}
	public void setTotalPages(long totalPages) {
		this.totalPages = totalPages;
	}
	public void setLast(boolean last) {
		this.last = last;
	}
}
