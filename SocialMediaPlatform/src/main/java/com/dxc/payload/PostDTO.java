package com.dxc.payload;

import com.dxc.entity.User;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PostDTO {
	private Long id;
	private String title;
	private String caption;
	private String hyperlink;
	private Integer views;
	private User user;
}
