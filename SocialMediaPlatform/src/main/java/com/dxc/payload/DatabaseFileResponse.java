package com.dxc.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DatabaseFileResponse {
	private String fileName;
	private String fileDownload;
	private String fileType;
	private long size;
	
}
