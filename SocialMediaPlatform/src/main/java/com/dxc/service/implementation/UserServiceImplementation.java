package com.dxc.service.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.dxc.entity.User;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.UserDTO;
import com.dxc.payload.UserResponse;
import com.dxc.repository.UserRepository;
import com.dxc.service.UserServiceInterface;

@Service
public class UserServiceImplementation implements UserServiceInterface {
	@Autowired
	private UserRepository userRepo;

	// Convert Entity to DTO
	private UserDTO mapToDTO(User user) {
		UserDTO userDTO = new UserDTO();
		userDTO.setId(user.getId());
		userDTO.setUsername(user.getUsername());
		userDTO.setPassword(user.getPassword());
		userDTO.setEmail(user.getEmail());

		return userDTO;
	}

	// Convert DTO to Entity
	public User mapToEntity(UserDTO userDTO) {
		User user = new User();
		user.setUsername(userDTO.getUsername());
		user.setPassword(userDTO.getPassword());
		user.setEmail(userDTO.getEmail());

		return user;
	}

	// Create User;
	@Override
	public UserDTO createUser(UserDTO userDTO) {
		User user = mapToEntity(userDTO);
		User newUser = userRepo.save(user);

		UserDTO userResponse = mapToDTO(newUser);

		return userResponse;
	}

	@Override
	public UserResponse getAllUsers(int pageNo, int pageSize, String sortBy, String sortDir) {
		Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).ascending()
				: Sort.by(sortBy).descending();

		Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
		Page<User> users = userRepo.findAll(pageable);

		List<User> listOfUsers = users.getContent();
		List<UserDTO> content = (List<UserDTO>) listOfUsers.stream().map(user -> mapToDTO(user))
				.collect(Collectors.toList());
		UserResponse userResponse = new UserResponse();

		userResponse.setContent(content);
		userResponse.setPageNo(users.getNumber());
		userResponse.setPageSize(users.getSize());
		userResponse.setTotalElements(users.getTotalElements());
		userResponse.setLast(users.isLast());

		return userResponse;
	}

	@Override
	public UserDTO getUserById(Long userId) {
		User user = userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("USER", "id", userId));
		return mapToDTO(user);
	}

	@Override
	public UserDTO updateUser(UserDTO userDTO, Long userId) {
		User user = userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("USER", "id", userId));
		user.setUsername(userDTO.getUsername());
		user.setEmail(userDTO.getEmail());

		User updatedUser = userRepo.save(user);

		return mapToDTO(updatedUser);
	}

	@Override
	public void deleteUserById(Long userId) {
		User user = userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("USER", "id", userId));
		userRepo.delete(user);
	}

//	@Override
//	public void updateResetPasswordToken(String token, String email) {
//		User user = userRepo.findByEmail(email)
//				.orElseThrow(() -> new ResourceNotFoundException("USER", "email", email));
//
//		user.setResetPasswordToken(token);
//		userRepo.save(user);
//	}
//
//	@Override
//	public User getByResetPasswordToken(String token) {
//		return userRepo.findByResetPasswordToken(token);
//	}
//
//	@Override
//	public void updatePassword(User user, String newPassword) {
//		BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
//		String encodedPassword = passwordEncoder.encode(newPassword);
//		user.setPassword(encodedPassword);
//
//		user.setResetPasswordToken(null);
//		userRepo.save(user);
//	}

}
