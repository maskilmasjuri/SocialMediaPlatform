package com.dxc.service.implementation;

import java.io.IOException;
import java.util.stream.Stream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.dxc.entity.DatabaseFile;
import com.dxc.entity.Post;
import com.dxc.entity.User;
import com.dxc.exception.FileNotFoundException;
import com.dxc.exception.FileStorageException;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.UserDTO;
import com.dxc.repository.DatabaseFileRepository;
import com.dxc.repository.PostRepository;
import com.dxc.service.DatabaseFileServiceInterface;

@Service
public class DatabaseFileServiceImplementation implements DatabaseFileServiceInterface {
	@Autowired
	private DatabaseFileRepository dbFileRepo;

	@Autowired
	private PostRepository postRepo;

	@Override
	public DatabaseFile storeFile(MultipartFile file, Long postId) {
		String fileName = StringUtils.cleanPath(file.getOriginalFilename());
		try {
			if (fileName.contains("..")) {
				throw new FileStorageException("Sorry! FileName contains invalid path Sequence" + fileName);
			}
			Post post = postRepo.findById(postId)
					.orElseThrow(() -> new ResourceNotFoundException("POST", "id", postId));
			DatabaseFile dbFile = new DatabaseFile(fileName, file.getContentType(), file.getBytes());
			dbFile.setPost(post);
			return dbFileRepo.save(dbFile);
		} catch (IOException e) {
			throw new FileStorageException("Could not Store File" + fileName + "please try again", e);
		}

	}

	@Override
	public DatabaseFile getFile(String fileId) {
		return dbFileRepo.findById(fileId)
				.orElseThrow(() -> new FileNotFoundException("File Not found with ID" + fileId));
	}
	
	@Override
	public DatabaseFile getFileByPostId(Long postId) {
//		User user = userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("USER", "id", userId));
//		return mapToDTO(user);
		
		String fileId = dbFileRepo.findByPostId(postId).getId();
		return dbFileRepo.findById(fileId)
				.orElseThrow(() -> new FileNotFoundException("File Not found with ID" + fileId));
	}
	
	@Override
	public Stream<DatabaseFile> getAllFiles() {
		return dbFileRepo.findAll().stream();
	}
}
