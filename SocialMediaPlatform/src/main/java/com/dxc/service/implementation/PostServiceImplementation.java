package com.dxc.service.implementation;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.dxc.entity.Post;
import com.dxc.exception.ResourceNotFoundException;
import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;
import com.dxc.repository.PostRepository;
import com.dxc.repository.UserRepository;
import com.dxc.service.PostServiceInterface;


// https://www.bezkoder.com/jpa-one-to-many/
@Service
public class PostServiceImplementation implements PostServiceInterface {
	@Autowired
	private PostRepository postRepo;
	@Autowired
	private UserRepository userRepo;
	
	// Convert Entity to DTO
		private PostDTO mapToDTO(Post post) {
			PostDTO postDTO = new PostDTO();
			postDTO.setId(post.getId());
			postDTO.setTitle(post.getTitle());
			postDTO.setCaption(post.getCaption());
			postDTO.setHyperlink(post.getHyperlink());
			postDTO.setViews(post.getViews());
			postDTO.setUser(post.getUser());

			return postDTO;
		}

		// Convert DTO to Entity
		public Post mapToEntity(PostDTO postDTO) {
			Post post = new Post();
			post.setTitle(postDTO.getTitle());
			post.setCaption(postDTO.getCaption());
			post.setHyperlink(postDTO.getHyperlink());
			post.setViews(postDTO.getViews());
			post.setUser(postDTO.getUser());

			return post;
		}
		
		// Create Post;
		@Override
		public PostDTO createPost(PostDTO postDTO, Long userId) {
			Post post = mapToEntity(postDTO);
			post.setUser(userRepo.findById(userId).orElseThrow(() -> new ResourceNotFoundException("USER", "id", userId)));
			post.setViews(0);
			Post newPost = postRepo.save(post);

			PostDTO postResponse = mapToDTO(newPost);

			return postResponse;
		}
		
		@Override
		public PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir) {
			Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).descending()
					: Sort.by(sortBy).ascending();
			
			Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
			Page<Post> posts = postRepo.findAll(pageable);
			
			List<Post> listOfPosts = posts.getContent();
			List<PostDTO> content = (List<PostDTO>) listOfPosts.stream().map(post -> mapToDTO(post))
					.collect(Collectors.toList());
			PostResponse postResponse = new PostResponse();
			
			postResponse.setContent(content);
			postResponse.setPageNo(posts.getNumber());
			postResponse.setPageSize(posts.getSize());
			postResponse.setTotalElements(posts.getTotalElements());
			postResponse.setTotalPages(posts.getTotalPages());
			postResponse.setLast(posts.isLast());
			
			return postResponse;
		}
		
		@Override
		public PostResponse getAllPostsByUserId(Long userId, int pageNo, int pageSize, String sortBy, String sortDir) {
			if(!userRepo.existsById(userId)) {
				throw new ResourceNotFoundException("USER", "id", userId);
			}
			Sort sort = sortDir.equalsIgnoreCase(Sort.Direction.ASC.name()) ? Sort.by(sortBy).descending()
					: Sort.by(sortBy).ascending();
			
			Pageable pageable = PageRequest.of(pageNo, pageSize, sort);
			
			Page<Post> posts = postRepo.findByUserId(userId, pageable);
			
			List<Post> listOfPosts = posts.getContent();
			List<PostDTO> content = (List<PostDTO>) listOfPosts.stream().map(post -> mapToDTO(post))
					.collect(Collectors.toList());
			PostResponse postResponse = new PostResponse();
			
			postResponse.setContent(content);
			postResponse.setPageNo(posts.getNumber());
			postResponse.setPageSize(posts.getSize());
			postResponse.setTotalElements(posts.getTotalElements());
			postResponse.setTotalPages(posts.getTotalPages());			
			postResponse.setLast(posts.isLast());
			
			return postResponse;
		}
		
		@Override
		public PostDTO updatePost(PostDTO postDTO, Long postId) {
			Post post = postRepo.findById(postId).orElseThrow(() -> new ResourceNotFoundException("POST", "id", postId));
			post.setTitle(postDTO.getTitle());
			post.setCaption(postDTO.getCaption());
			post.setHyperlink(postDTO.getHyperlink());
			
			Post updatedPost= postRepo.save(post);
			
			return mapToDTO(updatedPost);
		}
		
		@Override
		public PostDTO updatePostViews(Long postId) {
			Post post = postRepo.findById(postId).orElseThrow(() -> new ResourceNotFoundException("POST", "id", postId));
			post.setViews(post.getViews() + 1);
			
			Post updatedPost = postRepo.save(post);
			return mapToDTO(updatedPost);
		}
		
		@Override
		public void deletePost(Long postId) {
			Post post = postRepo.findById(postId).orElseThrow(() -> new ResourceNotFoundException("POST", "id", postId));
			postRepo.delete(post);
		}
		
		@Override
		public void deleteAllPostsByUser(Long userId) {
			if(!userRepo.existsById(userId)) {
				throw new ResourceNotFoundException("USER", "id", userId);
			}
			
			postRepo.deleteByUserId(userId);
		}
}
