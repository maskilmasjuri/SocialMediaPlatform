package com.dxc.service;

import com.dxc.entity.User;
import com.dxc.payload.UserDTO;
import com.dxc.payload.UserResponse;

public interface UserServiceInterface {

	UserDTO createUser(UserDTO userDTO);

	UserResponse getAllUsers(int pageNo, int pageSize, String sortBy, String sortDir);

	UserDTO getUserById(Long id);
	
	UserDTO updateUser(UserDTO userDTO, Long id);

	void deleteUserById(Long id);

//	void updateResetPasswordToken(String token, String email);
//
//	User getByResetPasswordToken(String token);
//
//	void updatePassword(User user, String newPassword);
//	
	
}
