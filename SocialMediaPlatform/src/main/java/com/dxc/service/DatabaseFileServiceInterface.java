package com.dxc.service;

import java.util.stream.Stream;

import org.springframework.web.multipart.MultipartFile;

import com.dxc.entity.DatabaseFile;

public interface DatabaseFileServiceInterface {

//	DatabaseFile storeFile(MultipartFile file);

	DatabaseFile getFile(String fileId);

	DatabaseFile storeFile(MultipartFile file, Long postId);

	Stream<DatabaseFile> getAllFiles();

	DatabaseFile getFileByPostId(Long postId);

}
