package com.dxc.service;

import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;

public interface PostServiceInterface {

	PostDTO createPost(PostDTO postDTO, Long userId);

	PostResponse getAllPosts(int pageNo, int pageSize, String sortBy, String sortDir);

	PostResponse getAllPostsByUserId(Long userId, int pageNo, int pageSize, String sortBy, String sortDir);

	PostDTO updatePost(PostDTO postDTO, Long postId);

	void deletePost(Long postId);

	void deleteAllPostsByUser(Long userId);

	PostDTO updatePostViews(Long postId);
	
}
