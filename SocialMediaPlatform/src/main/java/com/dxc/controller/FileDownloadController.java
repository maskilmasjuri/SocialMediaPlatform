package com.dxc.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dxc.entity.DatabaseFile;
import com.dxc.payload.DatabaseFileResponse;
import com.dxc.service.DatabaseFileServiceInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, allowCredentials="true")
@RequestMapping("/api")
public class FileDownloadController {
	@Autowired
	private DatabaseFileServiceInterface fileStorageService;

	@GetMapping("/downloadFile")
	public ResponseEntity<List<DatabaseFileResponse>> getListFiles() {
		List<DatabaseFileResponse> files = fileStorageService.getAllFiles().map(dbFile -> {
			String fileDownloadUri = ServletUriComponentsBuilder
					.fromCurrentContextPath()
					.path("/downloadFile/")
					.path(dbFile.getId())
					.toUriString();
			
			return new DatabaseFileResponse(
					dbFile.getFileName(), 
					fileDownloadUri, 
					dbFile.getFileType(),
					dbFile.getData().length);

		}).collect(Collectors.toList());

		return ResponseEntity.status(HttpStatus.OK).body(files);
	}
	
	@GetMapping
	("/downloadFile/{postid}")
	public ResponseEntity<Resource> downloadFileById(@PathVariable(value="postid") Long postId){
		DatabaseFile databaseFile = fileStorageService.getFileByPostId(postId);
		
		return ResponseEntity.ok().contentType(MediaType.parseMediaType(databaseFile.getFileType()))
				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\"" + databaseFile.getFileName() + "\"")
				.body(new ByteArrayResource(databaseFile.getData()));
	}

//	@GetMapping("/downloadFile/{fileName:.+}")
//	public ResponseEntity<Resource> downloadFile(@PathVariable String fileName, HttpServletRequest request) {
//
//		DatabaseFile databaseFile = fileStorageService.getFile(fileName);
//
//		return ResponseEntity.ok().contentType(MediaType.parseMediaType(databaseFile.getFileType()))
//				.header(HttpHeaders.CONTENT_DISPOSITION, "attachment:filename=\"" + databaseFile.getFileName() + "\"")
//				.body(new ByteArrayResource(databaseFile.getData()));
//	}
}
