package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.payload.PostDTO;
import com.dxc.payload.PostResponse;
import com.dxc.service.PostServiceInterface;
import com.dxc.utils.AppConstants;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, allowCredentials = "true")
@RequestMapping("/api")
public class PostController {
	@Autowired
	private PostServiceInterface postService;

	@PostMapping("/users/{userId}/posts")
	@PreAuthorize("authentication.principal.id == #userId")
	public ResponseEntity<PostDTO> createPost(@RequestBody PostDTO postDTO,
			@PathVariable(value = "userId") Long userId) {
		return new ResponseEntity<PostDTO>(postService.createPost(postDTO, userId), HttpStatus.CREATED);
	}

	@GetMapping("/posts")
	@PreAuthorize("hasRole('USER') or hasRole('ADMIN')")
	public PostResponse getAllPosts(
			@RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
		return postService.getAllPosts(pageNo, pageSize, sortBy, sortDir);
	}

	@GetMapping("/users/{userId}/posts")
	@PreAuthorize("authentication.principal.id == #userId or hasRole('ADMIN')")
	public PostResponse getAllPostsByUserId(@PathVariable(value = "userId") Long userId,
			@RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
		return postService.getAllPostsByUserId(userId, pageNo, pageSize, sortBy, sortDir);
	}

	@PutMapping("/posts/views/{id}")
	public ResponseEntity<PostDTO> updatePostViews(@PathVariable(name = "id") Long id){
		PostDTO postResponse = postService.updatePostViews(id);
		return new ResponseEntity<PostDTO>(postResponse, HttpStatus.OK);
	}
	
	@PutMapping("/posts/{id}")
	public ResponseEntity<PostDTO> updatePost(@RequestBody PostDTO postDTO, @PathVariable(name = "id") Long id) {
		PostDTO postResponse = postService.updatePost(postDTO, id);
		return new ResponseEntity<PostDTO>(postResponse, HttpStatus.OK);
	}

	@DeleteMapping("/posts/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> deletePostById(@PathVariable(name = "id") Long id) {
		postService.deletePost(id);
		return new ResponseEntity<String>("Post entity deleted successfully", HttpStatus.OK);
	}

	@DeleteMapping("/users/{userId}/posts")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> deleteAllPostsByUser(@PathVariable(name = "userId") Long id) {
		postService.deleteAllPostsByUser(id);
		return new ResponseEntity<String>("All posts by user deleted successfully", HttpStatus.OK);
	}
}
