package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.dxc.entity.DatabaseFile;
import com.dxc.payload.DatabaseFileResponse;
import com.dxc.service.DatabaseFileServiceInterface;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, allowCredentials="true")
@RequestMapping("/api")
public class FileUploadController {

	@Autowired
	private DatabaseFileServiceInterface fileStorageService;

	@PostMapping("/posts/{postId}/uploadFile")
	public DatabaseFileResponse uploadFile(@RequestParam("file") MultipartFile file, @PathVariable(value="postId") Long postId) {
		DatabaseFile fileName = fileStorageService.storeFile(file, postId);

		String fileDownloadUri = ServletUriComponentsBuilder.fromCurrentContextPath()
				.path("/downloadFile/")
				.path(fileName.getFileName())
				.toUriString();
		return new DatabaseFileResponse(fileName.getFileName(), fileDownloadUri, file.getContentType(), file.getSize());
	}
}
