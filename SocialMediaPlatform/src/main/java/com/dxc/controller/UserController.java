package com.dxc.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.dxc.payload.UserDTO;
import com.dxc.payload.UserResponse;
import com.dxc.service.UserServiceInterface;
import com.dxc.utils.AppConstants;

import net.bytebuddy.utility.RandomString;

@RestController
@CrossOrigin(origins = "http://localhost:4200", maxAge = 3600, allowCredentials="true")
@RequestMapping("/api")
public class UserController {
	@Autowired
	private UserServiceInterface userService;

	@PostMapping("/users")
	public ResponseEntity<UserDTO> createUser(@RequestBody UserDTO userDTO) {
		return new ResponseEntity<UserDTO>(userService.createUser(userDTO), HttpStatus.CREATED);
	}

	@GetMapping("/users")
	@PreAuthorize("hasRole('ADMIN')")
	public UserResponse getAllUsers(
			@RequestParam(value = "pageNo", defaultValue = AppConstants.DEFAULT_PAGE_NUMBER, required = false) int pageNo,
			@RequestParam(value = "pageSize", defaultValue = AppConstants.DEFAULT_PAGE_SIZE, required = false) int pageSize,
			@RequestParam(value = "sortBy", defaultValue = AppConstants.DEFAULT_SORT_BY, required = false) String sortBy,
			@RequestParam(value = "sortDir", defaultValue = AppConstants.DEFAULT_SORT_DIRECTION, required = false) String sortDir) {
		return userService.getAllUsers(pageNo, pageSize, sortBy, sortDir);
	}

	// Get user by id
	@GetMapping("/users/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<UserDTO> getUserById(@PathVariable(name = "id") Long id) {
		return ResponseEntity.ok(userService.getUserById(id));
	}

	// Update user by ID
	@PutMapping("/users/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<UserDTO> updateUser(@RequestBody UserDTO userDTO, @PathVariable(name = "id") Long id) {
		UserDTO userResponse = userService.updateUser(userDTO, id);
		return new ResponseEntity<UserDTO>(userResponse, HttpStatus.OK);
	}

	// Delete user by ID
	@DeleteMapping("/users/{id}")
	@PreAuthorize("hasRole('ADMIN')")
	public ResponseEntity<String> deleteUserById(@PathVariable(name = "id") Long id) {
		userService.deleteUserById(id);
		return new ResponseEntity<String>("User entity deleted successfully", HttpStatus.OK);
	}
	
	// Forgot password
//	@PostMapping("/users/forgot_password")
//	public void processForgotPassword(@RequestBody String email) {
//		String token = RandomString.make(30);
//		userService.updateResetPasswordToken(token, email);
//		String resetPasswordLink = Utility.
//	}
}
