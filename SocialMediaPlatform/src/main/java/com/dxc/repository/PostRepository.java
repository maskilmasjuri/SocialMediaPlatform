package com.dxc.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.dxc.entity.Post;

@Repository
public interface PostRepository extends JpaRepository<Post, Long>{
	Page<Post> findByUserId(Long userId,Pageable pageable);
	
	
	@Transactional
	void deleteByUserId(Long userId);
}
